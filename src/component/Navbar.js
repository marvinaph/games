import React from "react";
import { Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, UncontrolledCollapse} from 'reactstrap'
import { Link } from 'react-router-dom'

const NavBar = () => {

        return (
            <Navbar class="navbar" dark expand="lg">
                <NavbarBrand href="/">LOGO</NavbarBrand>
                <NavItem className="px-2 list-unstyled">
                    <Link to="/leaderboard">LEADERBOARD</Link>
                </NavItem>
                
                <NavbarToggler id="toggler" /><UncontrolledCollapse toggler="#toggler" navbar>
                    
                    <Nav className="justify-content-end" style={{ width: "100%" }} navbar>
                        
                        <NavItem className="px-2" >
                            <Link to="/login">LOGIN</Link>
                        </NavItem>
                        <NavItem className="text-center text-md-right">
                            <Link to="/register">REGISTER</Link>
                        </NavItem>
                    </Nav>
                </UncontrolledCollapse>
            </Navbar>
        )
}

export default NavBar;
