import './App.css';
import React, { Component } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import NavBar from './component/Navbar';
import Footer from './component/Footer';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Profile from './pages/Profile';
import Editprofile from './pages/Editprofile.js';
import Profile1 from './pages/Profile1';

import GameList from './pages/GameList';
import GameDetail from './pages/GameDetail/GameDetail';
import GameSuit from './pages/GameSuit';
import Leaderboard from './pages/leaderboard/leaderboard';

function App() {
	return (
		<div className="App">
			<Router>
				<NavBar />
				<Routes>
					<Route exact path="/" element={<Home />} />
					<Route exact path="/register" element={<Register />} />
					<Route exact path="/login" element={<Login />} />
					<Route exact path="/profile" element={<Profile />} />
					<Route exact path="/editprofile" element={<Editprofile />} />
					<Route exact path="/profile1" element={<Profile1 />} />
					<Route exact path="/gamelist" element={<GameList />} />
					<Route exact path="/gamedetail" element={<GameDetail />} />
					<Route exact path="/gamesuit" element={<GameSuit />} />
					<Route exact path="/leaderboard" element={<Leaderboard />} />
				</Routes>
				<Footer />
			</Router>
		</div>
	);
}

export default App;
