import React from "react";


const Profile1 = () => {
  return (
    <div>
      <form className="card">

        <h1>John Henry</h1>
        <p className="title">CEO &amp; Founder, Example</p>
        <p>Harvard University</p>

        <p>
          <button>Contact</button>
        </p>
      </form>
    </div>
  );
};
export default Profile1;
