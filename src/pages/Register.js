import React from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import styles from './Pages.css';

const Register = () => {
	return (
		<div className="register-box">
			<h1>REGISTER A NEW PLAYER</h1>
			<Form>
				<FormGroup className="px-4">
					<Label for="Username">Username</Label>
					<Input type="text" name="username" id="exampleEmail" placeholder="Enter Username" />
				</FormGroup>
				<FormGroup className="px-4">
					<Label for="Email">Email</Label>
					<Input type="email" name="email" id="exampleEmail" placeholder="Enter Email" />
				</FormGroup>
				<FormGroup className="px-4">
					<Label for="Password">Password</Label>
					<Input type="password" name="password" id="examplePassword" placeholder="Enter Password" />
				</FormGroup>

				<Button className="button">Register</Button>
			</Form>
		</div>
	);
};

export default Register;
