import React, { Component } from "react";
import firebase from "../services/firebase";

export default class Register extends Component {
  state = {}
  set = name => e => {
    this.setState({
      [name]: e.target.value
    })
  }
  handleRegister = e => {
    const { email, password } = this.state
    e.preventDefault()
    if (!email || !password) return alert('Please insert missing credentials')
    firebase.auth.createUserWithEmailAndPassword(email, password)
  }

  render() {
    return (
      <form onSubmit={this.handleRegister}>
        <input type="email" onChange={this.set('email')} />
        <input type="password" onChange={this.set('password')} />
        <input type="submit" />
      </form>
    );
  }
}

