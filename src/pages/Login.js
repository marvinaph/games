import React, { Component } from 'react';
import { Button, Form, Container, Row, Col } from 'react-bootstrap';
import './Login.css';

class Login extends Component {
	render() {
		return (
			<div>
				<Container className="nama">
					<Row>
						<Col lg={4} md={6} sm={12} className="box p-5 m-auto shadow-sm">
							<h1 className="login"> Sign In</h1>
							<Form>
								<Form.Group className="label mb-3" controlId="formBasicEmail">
									<Form.Label>Email address</Form.Label>
									<Form.Control type="email" placeholder="Enter email" />
								</Form.Group>

								<Form.Group className="label mb-3" controlId="formBasicPassword">
									<Form.Label>Password</Form.Label>
									<Form.Control type="password" placeholder="Password" />
								</Form.Group>

								<Button variant="primary" type="submit">
									Submit
								</Button>
							</Form>
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}

export default Login;
