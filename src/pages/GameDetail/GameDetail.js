import React from "react";
import img1 from "./group.png"
import "./Game.css"
import { Button } from "reactstrap";

const GameDetail = () => {
    return (
      <div className="detail">
        <div className="row">
          <div className="col">
            <img src={img1} alt=""/>
          </div>
          <div className="col">
            <ul>
              <h1>LET'S PLAY</h1>
              <h2>Rock-Paper-Scissors </h2>
              <li><Button color="primary" size="lg" href="/gamesuit"><h4> Play Game </h4></Button></li>
              <li><Button color="info" size="lg" href="/leaderboard">Leaderboard</Button></li>
            </ul>
            
          </div>
        </div>
        
      </div>
    );
}

export default GameDetail;