import React from "react";
import { Link } from "react-router-dom";
import "../Profile.css"

const Profile = () => {
  return (
    <div className="container">
      <form className="card">
        <h1 className="title">Username: Batman</h1>
        <p className="text mt-5">Email: batman@gmail.com</p>
        <p className="text">Score: 9000</p>
        <p className="text mb-5">Rank: 1</p>
        <div className="btnContainer">
          <Link to="/Editprofile">
            <button>Edit</button>
          </Link>
          <Link to="#">
            <button>All Players</button>
          </Link>
        </div>
      </form>
    </div>
  );
};
export default Profile;
